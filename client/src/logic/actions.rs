use crate::logic::{state, Perspective, PlayerEvent, State, WS_ID};
use crate::render::draw;
use crate::wasm::send;
use hanabi_shared::constants::{CODE_DIGIT_OPTIONS, CODE_LENGTH};
use hanabi_shared::{Card, Hint};
use wasm_bindgen::__rt::core::sync::atomic::Ordering::Relaxed;

type Function = Option<Box<dyn Fn()>>;

pub fn host() -> Function {
    Some(Box::new(|| {
        *state() = State::Host;
        draw();
    }))
}

pub fn join() -> Function {
    Some(Box::new(|| {
        *state() = State::Join([0; CODE_LENGTH]);
        draw();
    }))
}

pub fn host_new(players: usize) -> Function {
    Some(Box::new(move || {
        send(WS_ID.load(Relaxed), &format!("host {}", players));
    }))
}

pub fn join_delete(i: usize) -> Function {
    Some(Box::new(move || {
        if let State::Join(code) = state() {
            code[i] = 0;
            draw();
        }
    }))
}

pub fn join_input(i: usize) -> Function {
    Some(Box::new(move || {
        if let State::Join(code) = state() {
            for j in 0..CODE_LENGTH {
                if code[j] == 0 {
                    code[j] = i + 1;
                    break;
                }
            }
            draw();
            if let Some(game_id) = get_code(code) {
                send(WS_ID.load(Relaxed), &format!("spectate {}", game_id));
            }
        }
    }))
}

pub fn get_code(code: &[usize; CODE_LENGTH]) -> Option<usize> {
    let mut game_id = 0;
    for i in 0..CODE_LENGTH {
        if code[i] == 0 {
            return None;
        } else {
            game_id = game_id * CODE_DIGIT_OPTIONS + code[i] - 1;
        }
    }
    Some(game_id)
}

pub fn click_table(me: usize, player_count: usize) -> Function {
    Some(Box::new(move || {
        Perspective::Table { me, player_count }.set();
    }))
}

pub fn click_player(player_index: usize, player_count: usize) -> Function {
    Some(Box::new(move || {
        Perspective::Player {
            player: player_index,
            player_count,
        }
        .set();
    }))
}

pub fn click_main(me: usize, player_count: usize) -> Box<dyn Fn()> {
    Box::new(move || Perspective::Main { me, player_count }.set())
}

pub fn hint(player_index: usize, hint: Hint) -> Function {
    Some(Box::new(move || {
        let message = match hint {
            Hint::Color(c) => format!("hint {} color {}", player_index, c),
            Hint::Number(n) => format!("hint {} number {}", player_index, n),
        };
        send(WS_ID.load(Relaxed), &message);
    }))
}

pub fn join_game(i: usize) -> Function {
    Some(Box::new(move || {
        send(WS_ID.load(Relaxed), &format!("join {}", i));
    }))
}

pub fn on_complete_play(
    player_index: usize,
    card_index: usize,
    new_card: Option<Card>,
    discard: bool,
) -> Function {
    Some(Box::new(move || {
        if let State::Game { game, .. } = state() {
            game.play_2(player_index, card_index, new_card, discard);
        }
    }))
}

pub fn on_complete_draw(player_index: usize) -> Function {
    Some(Box::new(move || {
        if let State::Game { game, .. } = state() {
            game.players[player_index].event = PlayerEvent::None;
        }
    }))
}
