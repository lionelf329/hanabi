use crate::logic::State;

static mut STATE: State = State::InitialLoad;

pub fn state() -> &'static mut State {
    unsafe { &mut STATE }
}
