use crate::constants::layout::{
    CARD_HEIGHT, OVERVIEW_HEIGHT, TABLE_HEIGHT, TABLE_RADIUS, VIEW_HEIGHT,
};
use crate::logic::{state, State};
use crate::render::transform::{base_player_transform, radial};
use graphics::{Point, Vector};

#[derive(Copy, Clone, Debug)]
pub enum Perspective {
    Main { me: usize, player_count: usize },
    Player { player: usize, player_count: usize },
    Table { me: usize, player_count: usize },
}

impl Perspective {
    // Returns up, origin, gaze
    pub fn camera(&self) -> (Vector, Point, Point) {
        match self {
            Perspective::Main { me, player_count } => {
                let t = base_player_transform(*me, *player_count);
                let up = Vector::new(0.0, 1.0, 0.0);
                let origin = t * Point::new(0.0, VIEW_HEIGHT, 0.0);
                let gaze = Point::new(0.0, 0.0, 0.0);
                (up, origin, gaze)
            }
            Perspective::Player {
                player,
                player_count,
            } => {
                let t = base_player_transform(*player, *player_count);
                let up = Vector::new(0.0, 1.0, 0.0);
                let origin = Point::new(0.0, VIEW_HEIGHT, 0.0);
                let gaze = t * Point::new(0.0, CARD_HEIGHT, 0.0);
                (up, origin, gaze)
            }
            Perspective::Table { me, player_count } => {
                let angle = *me as f64 / *player_count as f64;
                let t = radial(TABLE_RADIUS, angle);
                let up = Vector::new(0.0, 1.0, 0.0);
                let origin = t * Point::new(0.0, OVERVIEW_HEIGHT, 0.0);
                let gaze = Point::new(0.0, TABLE_HEIGHT, 0.0);
                (up, origin, gaze)
            }
        }
    }

    pub fn set(self) {
        if let State::Game { camera, .. } = state() {
            camera.change_perspective(self);
        }
    }
}
