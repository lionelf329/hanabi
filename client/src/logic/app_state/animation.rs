use crate::constants::ANIMATION_STEP_COUNT;

pub enum Animation<T> {
    /// Represents a fixed object
    Fixed(T),
    /// Represents an animating object, moving linearly from start to end, where on_complete
    /// will be called when it finishes. If another animation interrupts it, it will be called
    /// early.
    Animating {
        start: T,
        end: T,
        progress: usize,
        on_complete: Option<Box<dyn Fn()>>,
    },
}

impl<T, U> Animation<T>
where
    T: Copy + std::ops::Sub<T, Output = U> + std::ops::Add<U, Output = T>,
    U: std::ops::Mul<f64, Output = U>,
{
    pub fn animating(start: T, end: T, on_complete: Option<Box<dyn Fn()>>) -> Animation<T> {
        Animation::Animating {
            start,
            end,
            progress: 0,
            on_complete,
        }
    }

    /// Calculates the current position as a weighted average of the two points
    pub fn position(&self) -> T {
        match self {
            Animation::Fixed(v) => *v,
            Animation::Animating {
                start,
                end,
                progress,
                ..
            } => *start + (*end - *start) * (*progress as f64 / ANIMATION_STEP_COUNT as f64),
        }
    }

    /// Progress through the animation, on a scale of 0.0 to 1.0
    pub fn progress(&self) -> f64 {
        match self {
            Animation::Fixed(_) => 0.0,
            Animation::Animating { progress, .. } => *progress as f64 / ANIMATION_STEP_COUNT as f64,
        }
    }

    /// If it was animating, and it had an on_complete function, this method calls it
    fn try_complete(&mut self) {
        if let Animation::Animating {
            progress,
            on_complete,
            ..
        } = self
        {
            if *progress < ANIMATION_STEP_COUNT {
                if let Some(f) = on_complete {
                    f();
                }
            }
        }
    }

    pub fn animate_to(&mut self, target: T, on_complete: Option<Box<dyn Fn()>>) {
        self.try_complete();
        let start = self.position();
        *self = Animation::Animating {
            start,
            end: target,
            progress: 0,
            on_complete,
        };
    }

    /// Returns true if an animation step occurred, and false if the object doesn't move
    pub fn tick(&mut self) -> bool {
        if let Animation::Animating { end, progress, .. } = self {
            *progress += 1;
            if *progress == ANIMATION_STEP_COUNT {
                let mut old_self = Animation::Fixed(*end);
                std::mem::swap(self, &mut old_self);
                if let Animation::Animating { on_complete, .. } = old_self {
                    if let Some(f) = on_complete {
                        f();
                    }
                }
            }
            true
        } else {
            false
        }
    }
}
