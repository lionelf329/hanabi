use crate::logic::{Game, Player};
use hanabi_shared::constants::{CODE_DIGIT_OPTIONS, CODE_LENGTH};
use hanabi_shared::{Card, Hint};

pub enum WsAction {
    Spectate([usize; CODE_LENGTH], Vec<bool>),
    Join(usize, bool),
    Leave(usize),
    StartGame(Game),
    Play(usize, Option<Card>, bool),
    Hint(usize, Hint),
}

pub fn parse(message: String) -> Option<WsAction> {
    if message == "" {
        return None;
    }
    let msg: Vec<&str> = message.split_whitespace().collect();
    match msg[0] {
        "spectate" => {
            let mut game_id = msg[1].parse::<usize>().unwrap();
            let mut players = Vec::new();
            for i in 2..msg.len() {
                players.push(msg[i] == "1");
            }
            let mut code = [0; CODE_LENGTH];
            for i in 0..CODE_LENGTH as usize {
                code[CODE_LENGTH - i - 1] = game_id % CODE_DIGIT_OPTIONS + 1;
                game_id /= CODE_DIGIT_OPTIONS;
            }
            Some(WsAction::Spectate(code, players))
        }
        "join" => {
            let position = msg[1].parse::<usize>().unwrap();
            Some(WsAction::Join(position, true))
        }
        "join_other" => {
            let position = msg[1].parse::<usize>().unwrap();
            Some(WsAction::Join(position, false))
        }
        "leave" => {
            let position = msg[1].parse::<usize>().unwrap();
            Some(WsAction::Leave(position))
        }
        "start_game" => {
            // "hints fuses turn deck_size red yellow green blue white rainbow  11 12  13 14 x"
            let hints = msg[1].parse::<usize>().unwrap();
            let fuses = msg[2].parse::<usize>().unwrap();
            let turn = msg[3].parse::<usize>().unwrap();
            let remaining = msg[4].parse::<usize>().unwrap();
            let mut stacks = Vec::new();
            let mut i = 5;
            while msg[i] != "|" {
                stacks.push(msg[i].parse::<usize>().unwrap());
                i += 1;
            }
            i += 1;
            let mut players = Vec::new();
            let mut player = Vec::new();
            while msg[i] != "x" {
                if msg[i] == "|" {
                    players.push(Player::new(player));
                    player = Vec::new();
                } else {
                    player.push(Card::from(msg[i]));
                }
                i += 1;
            }
            i += 1;
            players.push(Player::new(player));
            let mut discarded = Vec::new();
            while i < msg.len() {
                discarded.push(Card::from(msg[i]));
                i += 1;
            }
            Some(WsAction::StartGame(Game {
                code: [0; CODE_LENGTH],
                me: 0,
                remaining,
                players,
                discarded,
                stacks,
                hints,
                fuses,
                turn,
            }))
        }
        "play" => {
            let index = msg[1].parse::<usize>().unwrap();
            let new_card = if msg.len() == 4 && msg[2] == "draw" {
                Some(Card::from(msg[3]))
            } else {
                None
            };
            Some(WsAction::Play(index, new_card, false))
        }
        "discard" => {
            let index = msg[1].parse::<usize>().unwrap();
            let new_card = if msg.len() == 4 && msg[2] == "draw" {
                Some(Card::from(msg[3]))
            } else {
                None
            };
            Some(WsAction::Play(index, new_card, true))
        }
        "hint" => {
            let target_index = msg[1].parse::<usize>().unwrap();
            let value = msg[3].parse::<usize>().unwrap();
            let hint = match msg[2] {
                "color" => Hint::Color(value),
                "number" => Hint::Number(value),
                _ => return None,
            };
            Some(WsAction::Hint(target_index, hint))
        }
        _ => None,
    }
}
