use crate::logic::actions::get_code;
use crate::logic::sync::parse_action::WsAction;
use crate::logic::{state, CameraState, Perspective, PlayerEvent, State};
use crate::render::draw;
use crate::wasm::{close, send, start};
use hanabi_shared::constants::WEBSOCKET_ROUTE;
use std::sync::atomic::{AtomicUsize, Ordering::Relaxed};
use wasm_bindgen::__rt::core::sync::atomic::AtomicBool;
use web_sys::ErrorEvent;

mod parse_action;

pub static WS_ID: AtomicUsize = AtomicUsize::new(0);
static WS_ALIVE: AtomicBool = AtomicBool::new(true);

lazy_static! {
    static ref URL: String = {
        let location = web_sys::window().unwrap().location();
        let protocol = match location.protocol().unwrap().as_str() {
            "http:" => "ws",
            "https:" => "wss",
            _ => panic!(),
        };
        format!(
            "{}://{}/{}",
            protocol,
            location.host().unwrap(),
            WEBSOCKET_ROUTE
        )
    };
}

pub fn open_ws() {
    let id = start(&URL, Box::new(&open), Box::new(&receive), Box::new(&error));
    WS_ID.store(id, Relaxed);
}

fn open(_ws: &web_sys::WebSocket) {
    console_log!("socket opened");
}

fn receive(message: String) {
    WS_ALIVE.store(true, Relaxed);
    let action = if let Some(action) = parse_action::parse(message) {
        action
    } else {
        return;
    };
    match action {
        WsAction::Spectate(code, players) => {
            *state() = State::Joined {
                code,
                players,
                me: None,
            };
            draw();
        }
        WsAction::Join(position, is_me) => {
            match state() {
                State::Joined { players, me, .. } => {
                    players[position] = true;
                    if is_me {
                        *me = Some(position);
                    }
                }
                State::Game { game, .. } => {
                    game.players[position].event = PlayerEvent::None;
                }
                _ => (),
            }
            draw();
        }
        WsAction::Leave(position) => {
            match state() {
                State::Joined { players, me, .. } => {
                    players[position] = false;
                    if me.is_some() && me.unwrap() == position {
                        *me = None;
                    }
                }
                State::Game { game, .. } => {
                    game.players[position].event = PlayerEvent::Disconnected;
                }
                _ => (),
            }
            draw();
        }
        WsAction::StartGame(mut game) => {
            if let State::Joined { me, code, .. } = state() {
                let me = me.unwrap();
                game.code = *code;
                game.me = me;
                let player_count = game.players.len();
                *state() = State::Game {
                    camera: CameraState::new(Perspective::Main { me, player_count }),
                    game,
                };
                draw();
            }
        }
        WsAction::Play(index, new_card, discard) => {
            if let State::Game { game, .. } = state() {
                game.play(index, new_card, discard);
                draw();
            }
        }
        WsAction::Hint(target_index, hint) => {
            if let State::Game { game, .. } = state() {
                game.hint(target_index, hint);
                draw();
            }
        }
    }
}

fn error(_: ErrorEvent) {}

pub fn ping() {
    if !WS_ALIVE.load(Relaxed) {
        console_log!("didn't receive ping back from server");
        recover();
        return;
    }
    WS_ALIVE.store(false, Relaxed);
    if !send(WS_ID.load(Relaxed), "") {
        console_log!("failed to send ping");
        recover();
        return;
    }
}

fn recover() {
    close(WS_ID.load(Relaxed));
    let id = start(
        &URL,
        Box::new(&reopen),
        Box::new(&receive),
        Box::new(&error),
    );
    WS_ID.store(id, Relaxed);
    WS_ALIVE.store(true, Relaxed);
}

fn reopen(_ws: &web_sys::WebSocket) {
    let ws = WS_ID.load(Relaxed);
    match state() {
        State::InitialLoad => (),
        State::Host => (),
        State::Join(_) => (),
        State::Joined { code, .. } => {
            send(ws, &format!("spectate {}", get_code(code).unwrap()));
        }
        State::Game { game, .. } => {
            send(ws, &format!("spectate {}", get_code(&game.code).unwrap()));
        }
    }
}
