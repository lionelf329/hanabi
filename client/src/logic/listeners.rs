use crate::logic::{state, PlayCardOutcome, PlayerEvent, State, WS_ID};
use crate::render::{draw, Category, SCENE};
use crate::wasm::send;
use std::sync::atomic::Ordering::Relaxed;

pub fn mouse_down(e: web_sys::MouseEvent) {
    on_start(e.client_x(), e.client_y());
}

pub fn mouse_move(e: web_sys::MouseEvent) {
    on_move(e.client_x(), e.client_y());
}

pub fn touch_start(e: web_sys::TouchEvent) {
    e.prevent_default();
    let touches = get_touches(e);
    if touches.len() == 1 {
        let touch_pos = touches[0];
        on_start(touch_pos.0, touch_pos.1);
    }
}

pub fn touch_move(e: web_sys::TouchEvent) {
    let touches = get_touches(e);
    if touches.len() == 1 {
        let touch_pos = touches[0];
        on_move(touch_pos.0, touch_pos.1);
    }
}

pub fn touch_end() {
    if let State::Game { game, .. } = state() {
        if let PlayerEvent::Action(i, outcome) = game.players[game.me].event {
            if let PlayCardOutcome::Play = outcome {
                send(WS_ID.load(Relaxed), &format!("play {}", i));
            } else if let PlayCardOutcome::Discard = outcome {
                send(WS_ID.load(Relaxed), &format!("discard {}", i));
            }
            game.players[game.me].event = PlayerEvent::None;
            draw();
        }
    }
}

fn on_start(x: i32, y: i32) {
    if let Some(s) = unsafe { SCENE.as_ref() } {
        s.click(x, y);
        let hit = s.check_coords(x, y);
        if let State::Game { game, .. } = state() {
            if let Some(Category::OwnCard(card)) = hit {
                game.players[game.me].event = PlayerEvent::Action(card, PlayCardOutcome::Cancel);
                draw();
            }
        }
    }
}

fn on_move(x: i32, y: i32) {
    if let Some(s) = unsafe { SCENE.as_ref() } {
        if let State::Game { game, .. } = state() {
            if let PlayerEvent::Action(card, old_outcome) = game.players[game.me].event {
                let hit = s.check_coords(x, y);
                let outcome = match hit {
                    Some(Category::Table) => PlayCardOutcome::Play,
                    Some(Category::Discard) => PlayCardOutcome::Discard,
                    _ => PlayCardOutcome::Cancel,
                };
                if outcome != old_outcome {
                    game.players[game.me].event = PlayerEvent::Action(card, outcome);
                    draw();
                }
            }
        }
    }
}

fn get_touches(e: web_sys::TouchEvent) -> Vec<(i32, i32)> {
    let mut touches = Vec::new();
    for t in 0..e.touches().length() {
        let t = e.touches().get(t).unwrap();
        touches.push((t.client_x(), t.client_y()));
    }
    touches
}
