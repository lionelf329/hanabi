use crate::constants::{layout, svg};
use crate::render::render_card::RenderCard;
use crate::scene::{Entity, Scene};
use graphics::transform::scale_all;
use graphics::Matrix;
use hanabi_shared::Card;

pub fn draw_deck<T: Copy>(scene: &mut Scene<T>, cards: usize, h: u32) {
    for i in 0..cards {
        scene.push(Entity::Svg {
            svg: Card::default().svg(false),
            t: deck_transform(i, h),
            use_camera: false,
            on_click: None,
            category: None,
        })
    }
}

fn deck_transform(i: usize, h: u32) -> Matrix {
    let r = (i % layout::DISCARDED_COL_LEN) as f64;
    let c = (i / layout::DISCARDED_COL_LEN) as f64;
    let x = c * layout::DISCARDED_SIZE;
    let y = h as f64 - (r + 1.0) * layout::DISCARDED_SIZE;
    scale_all(layout::DISCARDED_SIZE / svg::SIZE).translate(x, y, 0.0)
}
