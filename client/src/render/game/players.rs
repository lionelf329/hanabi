use crate::constants::{color, svg};
use crate::logic::{actions, Player, PlayerEvent};
use crate::render::render_card::RenderCard;
use crate::render::transform::{base_card_transform, base_player_transform};
use crate::render::Category;
use crate::scene::{Entity, Scene};
use crate::svg_tools::Svg;
use graphics::transform::{scale, scale_all, translate, translate_y};
use graphics::Matrix;
use hanabi_shared::{Card, ColorHintStatus, Hint};

pub fn draw_players(scene: &mut Scene<Category>, players: &Vec<Player>, me: usize, turn: usize) {
    let player_count = players.len();
    for (i, player) in players.iter().enumerate() {
        draw_player(scene, player, i == me, i == turn, i, player_count);
    }
}

fn draw_player(
    scene: &mut Scene<Category>,
    player: &Player,
    is_me: bool,
    active: bool,
    player_index: usize,
    player_count: usize,
) {
    let t = base_player_transform(player_index, player_count);
    let card_count = player.cards.len() as f64;
    let disconnected = if let PlayerEvent::Disconnected = player.event {
        true
    } else {
        false
    };
    if !is_me {
        draw_body(scene, active, disconnected, t);
        if card_count > 0.0 {
            scene.push(Entity::Svg {
                svg: Svg::solid(&svg::SQUARE, "#0000"),
                t: player_hitbox_transform(player_index, player_count, card_count),
                use_camera: true,
                on_click: actions::click_player(player_index, player_count),
                category: None,
            });
        }
    }
    for (i_usize, &card) in player.cards.iter().enumerate() {
        let i = i_usize as f64;
        let (card, t, disabled) = match &player.event {
            PlayerEvent::ReceiveHint(hint) if card.check_hint(*hint) => match hint {
                Hint::Color(c) => get_hint((i, card_count, card, is_me, t), *c, scene),
                Hint::Number(_) => get_hint((i, card_count, card, is_me, t), 0, scene),
            },
            PlayerEvent::ReceiveHint(_) => get_default((i, card_count, card, is_me, t)),
            PlayerEvent::Action(action_index, outcome) if i_usize == *action_index => {
                get_action((i, card_count, card, is_me, t), outcome.color())
            }
            PlayerEvent::Action(..) => get_default((i, card_count, card, is_me, t)),
            PlayerEvent::PlayCard {
                card: card_index,
                animation,
                discard,
                ..
            } if i_usize == *card_index => {
                get_play_card(card, is_me && *discard, animation.position())
            }
            PlayerEvent::PlayCard {
                card: card_index,
                animation,
                will_draw: draw,
                ..
            } => {
                let p = animation.progress();
                let card_count = if *draw { card_count } else { card_count - p };
                let i = if i_usize <= *card_index { i } else { i - p };
                get_default((i, card_count, card, is_me, t))
            }
            PlayerEvent::DrawCard { animation } if i_usize == player.cards.len() - 1 => {
                let card = if is_me { Card::default() } else { card };
                let t1 = translate_y(animation.position());
                let t2 = base_card_transform(i, card_count, is_me);
                (card, t * t2 * t1, false)
            }
            PlayerEvent::DrawCard { .. } => get_default((i, card_count, card, is_me, t)),
            PlayerEvent::Disconnected => get_default((i, card_count, card, is_me, t)),
            PlayerEvent::None => get_default((i, card_count, card, is_me, t)),
        };
        let category = if is_me {
            Some(Category::OwnCard(i_usize))
        } else {
            None
        };
        card.add_to_scene(scene, t, None, disabled, category);
    }
}

fn draw_body<T: Copy>(scene: &mut Scene<T>, active: bool, disconnected: bool, t: Matrix) {
    let t1 = translate(-svg::SIZE / 2.0, -svg::SIZE / 2.0, 0.0);
    let t2 = scale_all(10.0);
    let t3 = translate_y(60.0);
    let color = if disconnected {
        color::FUSE_RED
    } else if active {
        color::PLAYER_ACTIVE
    } else {
        color::PLAYER_DEFAULT
    };
    let t = t * t3 * t2 * t1;
    scene.push(Entity::Svg {
        svg: Svg::solid(&svg::PLAYER, color),
        t,
        use_camera: true,
        on_click: None,
        category: None,
    });
}

fn player_hitbox_transform(player_index: usize, player_count: usize, card_count: f64) -> Matrix {
    scale(card_count + 2.0, 5.0, 1.0)
        .translate(0.0, -svg::SIZE, 0.0)
        .transform(base_card_transform(-1.0, card_count, false))
        .transform(base_player_transform(player_index, player_count))
}

type CardT = (Card, Matrix, bool);
type Args = (f64, f64, Card, bool, Matrix);

/// This function handles the case where the card is currently targeted by a hint
fn get_hint<T: Copy>(
    (i, card_count, card, is_me, t): Args,
    c: usize,
    scene: &mut Scene<T>,
) -> CardT {
    let t2 = t * base_card_transform(i, card_count, is_me);
    scene.push(Entity::Svg {
        svg: color::card_color_svg(&svg::LOCATION, c, false),
        t: translate_y(svg::SIZE).transform(t2),
        use_camera: true,
        on_click: None,
        category: None,
    });
    get_default((i, card_count, card, is_me, t))
}

/// This function handles the case where the card is currently targeted by the player's action
fn get_action((i, card_count, card, is_me, t): Args, color: usize) -> CardT {
    let card = if is_me { Card::new(color, 0) } else { card };
    let t = t * base_card_transform(i, card_count, is_me);
    (card, t, false)
}

/// This function handles the case where the card is currently being played or discarded
fn get_play_card(card: Card, hide: bool, t: Matrix) -> CardT {
    if hide {
        hide_card_show_hint(card, t)
    } else {
        (card, t, false)
    }
}

/// This function handles the case where the card should be displayed normally
fn get_default((i, card_count, card, is_me, t): Args) -> CardT {
    let t = t * base_card_transform(i, card_count, is_me);
    if is_me {
        hide_card_show_hint(card, t)
    } else {
        (card, t, false)
    }
}

fn hide_card_show_hint(card: Card, t: Matrix) -> CardT {
    let (color, disabled) = match card.hint_color {
        ColorHintStatus::Known => (card.color, false),
        ColorHintStatus::Ambiguous(color) => (color, true),
        ColorHintStatus::SomeEliminated(_) => (0, false),
    };
    let number = if card.hint_number.is_known() {
        card.number
    } else {
        0
    };
    (Card::new(color, number), t, disabled)
}
