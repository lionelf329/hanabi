use crate::constants::{color::card_color_svg, layout, svg};
use crate::scene::{Entity, Scene};
use graphics::transform::scale_all;
use graphics::Matrix;
use hanabi_shared::{Card, ColorHintStatus, Hint, NumberHintStatus};

pub fn draw_hint_info<T: Copy>(scene: &mut Scene<T>, w: u32, card: Card) {
    let mut hints = Vec::with_capacity(10);
    if let NumberHintStatus::SomeEliminated(x) = card.hint_number {
        for (i, x) in x.iter().enumerate() {
            if *x {
                hints.push(Hint::Number(i + 1));
            }
        }
    }
    if let ColorHintStatus::SomeEliminated(x) = card.hint_color {
        for (i, x) in x.iter().enumerate() {
            if *x {
                hints.push(Hint::Color(i + 1));
            }
        }
    }
    for (i, h) in hints.iter().enumerate() {
        let svg = match h {
            Hint::Color(c) => card_color_svg(&svg::CIRCLE, *c, true),
            Hint::Number(n) => card_color_svg(&svg::CARD[*n], 0, true),
        };
        scene.push(Entity::Svg {
            svg,
            t: hint_info_transform(i, hints.len(), w),
            use_camera: false,
            on_click: None,
            category: None,
        })
    }
}

fn hint_info_transform(i: usize, hint_count: usize, w: u32) -> Matrix {
    let i = i as f64;
    let hint_count = hint_count as f64;
    let w = w as f64;
    let width = layout::HINT_FACTOR * svg::SIZE;
    scale_all(layout::HINT_FACTOR).translate_x(w / 2.0 + width * (i - hint_count / 2.0))
}
