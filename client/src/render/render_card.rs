use crate::constants::{color, svg};
use crate::scene::{Entity, Scene};
use crate::svg_tools::Svg;
use graphics::transform::scale_all;
use graphics::{Matrix, Mesh, Object, Shape};
use hanabi_shared::Card;

pub trait RenderCard {
    fn svg(&self, disabled: bool) -> Svg;

    fn add_to_scene<T: Copy>(
        &self,
        scene: &mut Scene<T>,
        t: Matrix,
        on_click: Option<Box<dyn Fn()>>,
        disabled: bool,
        category: Option<T>,
    );
}

impl RenderCard for Card {
    fn svg(&self, disabled: bool) -> Svg {
        color::card_color_svg(&svg::CARD[self.number], self.color, disabled)
    }

    fn add_to_scene<T: Copy>(
        &self,
        scene: &mut Scene<T>,
        t: Matrix,
        on_click: Option<Box<dyn Fn()>>,
        disabled: bool,
        category: Option<T>,
    ) {
        scene.push(Entity::Shape(Object::new(
            Mesh {
                shape: Shape::Plane,
                du: 1.0,
                dv: 1.0,
            },
            color::CARD_BACKGROUND_MATERIAL,
            scale_all(14.0).translate(5.0, 5.0, 0.0).transform(t),
        )));
        scene.push(Entity::Shape(Object::new(
            Mesh {
                shape: Shape::Plane,
                du: 1.0,
                dv: 1.0,
            },
            color::CARD_BACKGROUND_MATERIAL,
            FLIP_PLANE
                .scale_all(14.0)
                .translate(5.0, 5.0, 0.0)
                .transform(t),
        )));
        scene.push(Entity::Svg {
            svg: self.svg(disabled),
            t,
            use_camera: true,
            on_click,
            category,
        });
    }
}

const FLIP_PLANE: Matrix = Matrix::new([
    [0.0, 1.0, 0.0, 0.0],
    [1.0, 0.0, 0.0, 0.0],
    [0.0, 0.0, 1.0, 0.0],
]);
