use crate::constants::{color, svg};
use crate::logic::actions;
use crate::scene::{Entity, Scene};
use crate::svg_tools::Svg;
use graphics::transform::{rotate_z, translate, translate_x};
use graphics::{Matrix, Point};
use hanabi_shared::constants::CODE_LENGTH;
use std::f64::consts::PI;

pub fn draw_joined<T: Copy>(
    w: u32,
    h: u32,
    code: &[usize; CODE_LENGTH],
    players: &Vec<bool>,
    me: Option<usize>,
) -> Scene<T> {
    let mut scene = Scene::new(None, None);
    let f = h as f64 / svg::SIZE / 5.0;
    for i in 0..players.len() {
        let color = if me.is_some() && me.unwrap() == i {
            color::HOST
        } else if players[i] {
            color::JOIN
        } else {
            color::GREY
        };
        scene.push(Entity::Svg {
            svg: Svg::solid(&svg::PLAYER, color),
            t: player_transform(i, players.len(), w, h, f),
            use_camera: false,
            on_click: actions::join_game(i),
            category: None,
        });
    }
    scene.push(Entity::Svg {
        svg: Svg::solid(&svg::CIRCLE, color::GREY),
        t: table_transform(w, h, f),
        use_camera: false,
        on_click: None,
        category: None,
    });
    for i in 0..CODE_LENGTH {
        scene.push(Entity::Svg {
            svg: Svg::solid(&svg::CARD[code[i]], color::JOIN),
            t: code_transform(i),
            use_camera: false,
            on_click: None,
            category: None,
        });
    }
    scene
}

fn table_transform(w: u32, h: u32, f: f64) -> Matrix {
    translate(-svg::SIZE / 2.0, -svg::SIZE / 2.0, 0.0)
        .scale_all(f * 2.4)
        .translate(w as f64 / 2.0, h as f64 / 2.0, 0.0)
}

fn player_transform(i: usize, player_count: usize, w: u32, h: u32, f: f64) -> Matrix {
    let r = rotate_z(-2.0 * PI * i as f64 / player_count as f64);
    let t = r * Point::new(0.0, -svg::SIZE * 1.5, 0.0);
    translate(-svg::SIZE / 2.0, -svg::SIZE / 2.0, 0.0)
        .translate(t[0], t[1], t[2])
        .scale_all(f)
        .translate(w as f64 / 2.0, h as f64 / 2.0, 0.0)
}

fn code_transform(i: usize) -> Matrix {
    translate_x(svg::SIZE * (i as f64)).scale_all(2.0)
}
