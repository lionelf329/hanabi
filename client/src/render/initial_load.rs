use crate::constants::{color, svg};
use crate::logic::actions;
use crate::scene::{Entity, Scene};
use crate::svg_tools::Svg;
use graphics::transform::translate_x;
use graphics::Matrix;

pub fn draw_initial_load<T: Copy>(w: u32, h: u32) -> Scene<T> {
    let mut scene = Scene::new(None, None);
    scene.push(Entity::Svg {
        svg: Svg::solid(&svg::HOST, color::HOST),
        t: initial_load_transform(w, h, 0),
        use_camera: false,
        on_click: actions::host(),
        category: None,
    });
    scene.push(Entity::Svg {
        svg: Svg::solid(&svg::JOIN, color::JOIN),
        t: initial_load_transform(w, h, 1),
        use_camera: false,
        on_click: actions::join(),
        category: None,
    });
    scene
}

fn initial_load_transform(w: u32, h: u32, i: usize) -> Matrix {
    let factor = w as f64 / 2.0 / 3.0;
    translate_x(svg::SIZE * (i as f64 * 3.0 + 1.0))
        .scale_all(factor / svg::SIZE)
        .translate_y((h as f64 - factor) / 2.0)
}
