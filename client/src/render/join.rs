use crate::constants::{color, svg};
use crate::logic::actions;
use crate::scene::{Entity, Scene};
use crate::svg_tools::Svg;
use graphics::transform::scale_all;
use graphics::Matrix;
use hanabi_shared::constants::{CODE_DIGIT_OPTIONS, CODE_LENGTH};

pub fn draw_join<T: Copy>(w: u32, h: u32, code: &[usize; CODE_LENGTH]) -> Scene<T> {
    let mut scene = Scene::new(None, None);
    for i in 0..CODE_LENGTH {
        scene.push(Entity::Svg {
            svg: Svg::solid(&svg::CARD[code[i]], color::GREY),
            t: code_transform(w, h, i),
            use_camera: false,
            on_click: actions::join_delete(i),
            category: None,
        });
    }
    for i in 0..CODE_DIGIT_OPTIONS {
        scene.push(Entity::Svg {
            svg: Svg::solid(&svg::CARD[i + 1], color::JOIN),
            t: input_transform(w, h, i),
            use_camera: false,
            on_click: actions::join_input(i),
            category: None,
        });
    }
    scene
}

fn code_transform(w: u32, h: u32, i: usize) -> Matrix {
    let factor = h as f64 / 4.0;
    scale_all(factor / svg::SIZE)
        .translate(w as f64 / 2.0, factor * 0.8, 0.0)
        .translate_x((i as f64 - (CODE_LENGTH as f64) / 2.0) * factor)
}

fn input_transform(w: u32, h: u32, i: usize) -> Matrix {
    let factor = h as f64 / 4.0;
    scale_all(factor / svg::SIZE)
        .translate(w as f64 / 2.0, factor * 2.2, 0.0)
        .translate_x((i as f64 - (CODE_DIGIT_OPTIONS as f64) / 2.0) * factor)
}
