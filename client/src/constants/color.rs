use crate::constants::svg;
use crate::svg_tools::{Svg, SvgCommand};
use graphics::{Material, Mesh, Reflectance, Shape};

pub const GREY: &str = "#888";
const RED: &str = "#f44336";
const YELLOW: &str = "#fbc02d";
const GREEN: &str = "#4caf50";
const BLUE: &str = "#2196f3";
const WHITE: &str = "#ccc";

const _RED: &str = "#b47874";
const _YELLOW: &str = "#b0976d";
const _GREEN: &str = "#708971";
const _BLUE: &str = "#678dad";
const _WHITE: &str = "#aaa";

pub fn card_color_svg(path: &'static [SvgCommand], color: usize, disabled: bool) -> Svg {
    if color < 6 {
        let colors = if disabled {
            [GREY, _RED, _YELLOW, _GREEN, _BLUE, _WHITE]
        } else {
            [GREY, RED, YELLOW, GREEN, BLUE, WHITE]
        };
        Svg::solid(path, colors[color])
    } else if color == 6 {
        let colors = if disabled {
            [_RED, _YELLOW, _GREEN, _BLUE]
        } else {
            [RED, YELLOW, GREEN, BLUE]
        };
        Svg::gradient(
            path,
            (0.0, svg::SIZE / 2.0),
            (24.0, svg::SIZE / 2.0),
            &[
                (0.1, colors[0]),
                (0.4, colors[1]),
                (0.7, colors[2]),
                (0.9, colors[3]),
            ],
        )
    } else {
        panic!()
    }
}

pub const CANVAS_COLOR: &str = "#aaa";

pub const HOST: &str = "#095";
pub const JOIN: &str = "#67f";

pub const FUSE_RED: &str = "#d44";
pub const FUSE_GREY: &str = GREY;

pub const HINT_BLUE: &str = "#66f";
pub const HINT_GREY: &str = GREY;

pub const CARD_BACKGROUND_MATERIAL: Material = Material {
    color: [100, 100, 100],
    reflectance: Reflectance {
        ambient: 0.1,
        diffuse: 0.6,
        specular: 1.0,
        specular_exponent: 5.0,
    },
};

pub const TABLE_MESH: Mesh = Mesh {
    shape: Shape::Circle,
    du: 0.125,
    dv: 1.0 / 30.0,
};
pub const TABLE_MATERIAL: Material = Material {
    color: [200, 120, 80],
    reflectance: Reflectance {
        ambient: 0.1,
        diffuse: 0.6,
        specular: 1.0,
        specular_exponent: 5.0,
    },
};

pub const PLAYER_DEFAULT: &str = "#888";
pub const PLAYER_ACTIVE: &str = "#099";
