#[derive(Copy, Clone)]
pub enum SvgToken {
    M(bool),
    L(bool),
    H(bool),
    V(bool),
    Q(bool),
    T(bool),
    C(bool),
    S(bool),
    A(bool),
    Z,
    Number(f64),
}

pub fn tokenize_svg(mut path: &str) -> Vec<SvgToken> {
    let mut tokens = Vec::new();
    while !path.is_empty() {
        let c = path.as_bytes()[0] as char;
        if c == '-' || c == '.' || c.is_numeric() {
            let mut dot = c == '.';
            let mut i = 1;
            while i < path.len() {
                let c = path.as_bytes()[i] as char;
                if !(c.is_numeric() || (c == '.' && !dot)) {
                    break;
                }
                dot |= c == '.';
                i += 1;
            }
            tokens.push(SvgToken::Number(path[..i].parse::<f64>().unwrap()));
            path = &path[i..];
        } else if c.is_alphabetic() {
            tokens.push(match (c.to_ascii_uppercase(), c.is_ascii_uppercase()) {
                ('M', u) => SvgToken::M(u),
                ('L', u) => SvgToken::L(u),
                ('H', u) => SvgToken::H(u),
                ('V', u) => SvgToken::V(u),
                ('Q', u) => SvgToken::Q(u),
                ('T', u) => SvgToken::T(u),
                ('C', u) => SvgToken::C(u),
                ('S', u) => SvgToken::S(u),
                ('A', u) => SvgToken::A(u),
                ('Z', _) => SvgToken::Z,
                _ => panic!("Invalid input"),
            });
            path = &path[1..];
        } else if c.is_ascii_whitespace() {
            path = &path[1..];
        } else {
            panic!("Invalid input");
        }
    }
    tokens
}
