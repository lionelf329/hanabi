use super::parser::SvgCommand;
use crate::wasm::Canvas;
use graphics::{Camera, Matrix, Point, Vector};
use web_sys::Path2d;

pub struct Svg {
    commands: &'static [SvgCommand],
    shader: SvgShader,
}

enum SvgShader {
    Solid(String),
    Gradient(Point, Point, Vec<(f32, String)>),
}

const SVG: Matrix = Matrix::new([
    [1.0, 0.0, 0.0, 0.0],
    [0.0, -1.0, 0.0, 24.0],
    [0.0, 0.0, 1.0, 0.0],
]);

impl Svg {
    pub fn solid(commands: &'static [SvgCommand], color: &str) -> Svg {
        Svg {
            commands,
            shader: SvgShader::Solid(String::from(color)),
        }
    }

    pub fn gradient(
        commands: &'static [SvgCommand],
        p1: (f64, f64),
        p2: (f64, f64),
        stops: &[(f32, &str)],
    ) -> Svg {
        let p1 = Point::new(p1.0, p1.1, 0.0);
        let p2 = Point::new(p2.0, p2.1, 0.0);
        let stops = stops.iter().map(|&x| (x.0, String::from(x.1))).collect();
        Svg {
            commands,
            shader: SvgShader::Gradient(p1, p2, stops),
        }
    }

    fn get_path(&self, mut t: Matrix, camera: Option<&Camera>) -> Option<Path2d> {
        if camera.is_some() {
            t = t * SVG;
        }
        let mut fail = false;
        let fail = &mut fail;
        let mut commands: Vec<SvgCommand> = self
            .commands
            .iter()
            .map(|&x| match x {
                SvgCommand::M(p) => SvgCommand::M(t * p),
                SvgCommand::L(p) => SvgCommand::L(t * p),
                SvgCommand::Q(p1, p2) => SvgCommand::Q(t * p1, t * p2),
                SvgCommand::T(p) => SvgCommand::T(t * p),
                SvgCommand::C(p1, p2, p3) => SvgCommand::C(t * p1, t * p2, t * p3),
                SvgCommand::S(p1, p2) => SvgCommand::S(t * p1, t * p2),
                SvgCommand::A(r, rotation, large_arc, sweep, p) => SvgCommand::A(
                    t * r,
                    rotation, // todo
                    large_arc,
                    sweep,
                    t * p,
                ),
                SvgCommand::Z => SvgCommand::Z,
            })
            .collect();
        if let Some(camera) = camera {
            commands = commands
                .iter()
                .map(|&x| match x {
                    SvgCommand::M(p) => SvgCommand::M(transform_point(&camera, fail, p)),
                    SvgCommand::L(p) => SvgCommand::L(transform_point(&camera, fail, p)),
                    SvgCommand::Q(p1, p2) => SvgCommand::Q(
                        transform_point(&camera, fail, p1),
                        transform_point(&camera, fail, p2),
                    ),
                    SvgCommand::T(p) => SvgCommand::T(transform_point(&camera, fail, p)),
                    SvgCommand::C(p1, p2, p3) => SvgCommand::C(
                        transform_point(&camera, fail, p1),
                        transform_point(&camera, fail, p2),
                        transform_point(&camera, fail, p3),
                    ),
                    SvgCommand::S(p1, p2) => SvgCommand::S(
                        transform_point(&camera, fail, p1),
                        transform_point(&camera, fail, p2),
                    ),
                    SvgCommand::A(r, rotation, large_arc, sweep, p) => SvgCommand::A(
                        transform_vector(&camera, r),
                        rotation, // todo
                        large_arc,
                        sweep,
                        transform_point(&camera, fail, p),
                    ),
                    SvgCommand::Z => SvgCommand::Z,
                })
                .collect();
        }
        if *fail {
            return None;
        }
        let commands: Vec<String> = commands.iter().map(|&x| format!("{}", x)).collect();
        Some(Path2d::new_with_path_string(&commands.join("")).unwrap())
    }

    pub fn draw(&self, canvas: &Canvas, t: Matrix, camera: Option<&Camera>) {
        match &self.shader {
            SvgShader::Solid(color) => canvas.set_fill_style(&color),
            SvgShader::Gradient(mut p1, mut p2, stops) => {
                p1 = t * p1;
                p2 = t * p2;
                let (p1, p2) = if let Some(camera) = camera {
                    let p1 = camera.world_to_pixel(p1);
                    let p2 = camera.world_to_pixel(p2);
                    (p1, p2)
                } else {
                    ([p1[0], p1[1]], [p2[0], p2[1]])
                };
                canvas.set_gradient(p1, p2, &stops[..]);
            }
        };
        match self.get_path(t, camera) {
            Some(p) => canvas.fill_with_path_2d(&p),
            None => (),
        }
    }
}

fn transform_point(camera: &Camera, fail: &mut bool, mut point: Point) -> Point {
    point = camera.world_to_viewing(point);
    if point[2] > 0.0 {
        *fail = true;
    }
    let [x, y] = camera.viewing_to_pixel(point);
    Point::new(x, y, 1.0)
}

fn transform_vector(camera: &Camera, mut vector: Vector) -> Vector {
    vector = camera.world_to_viewing(vector);
    let [x, y] = camera.viewing_to_pixel(vector.to_point());
    Vector::new(x, y, 0.0)
}
