use crate::svg_tools::lexer::{tokenize_svg, SvgToken};
use graphics::{Point, Vector};
use std::fmt::{Display, Formatter};

#[derive(Copy, Clone, Debug)]
pub enum SvgCommand {
    M(Point),
    L(Point),
    Q(Point, Point),
    T(Point),
    C(Point, Point, Point),
    S(Point, Point),
    A(Vector, f64, bool, bool, Point),
    Z,
}

impl Display for SvgCommand {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(&match self {
            SvgCommand::M(p) => format!("M {:.2} {:.2}", p[0], p[1]),
            SvgCommand::L(p) => format!("L {:.2} {:.2}", p[0], p[1]),
            SvgCommand::Q(p1, p2) => {
                format!("Q {:.2} {:.2} {:.2} {:.2}", p1[0], p1[1], p2[0], p2[1])
            }
            SvgCommand::T(p) => {
                format!("T {:.2} {:.2}", p[0], p[1])
            }
            SvgCommand::C(p1, p2, p3) => format!(
                "C {:.2} {:.2} {:.2} {:.2} {:.2} {:.2}",
                p1[0], p1[1], p2[0], p2[1], p3[0], p3[1]
            ),
            SvgCommand::S(p1, p2) => {
                format!("S {:.2} {:.2} {:.2} {:.2}", p1[0], p1[1], p2[0], p2[1])
            }
            SvgCommand::A(r, rotation, large_arc, sweep, p) => {
                let large_arc = if *large_arc { 1 } else { 0 };
                let sweep = if *sweep { 1 } else { 0 };
                format!(
                    "A {:.2} {:.2} {:.2} {} {} {:.2} {:.2}",
                    r[0], r[1], rotation, large_arc, sweep, p[0], p[1],
                )
            }
            SvgCommand::Z => String::from("Z"),
        })
    }
}

pub fn parse_svg(path: &str) -> Option<Vec<SvgCommand>> {
    let path = tokenize_svg(path);
    let mut point = (0.0, 0.0);
    let mut i = 0;
    let mut commands = Vec::new();
    while i < path.len() {
        i += 1;
        match path[i - 1] {
            SvgToken::M(true) => {
                let x = next_number(&mut i, &path).unwrap();
                let y = next_number(&mut i, &path).unwrap();
                point = (x, y);
                commands.push(SvgCommand::M(Point::new(x, y, 0.0)));
            }
            SvgToken::M(false) => {
                let x = next_number(&mut i, &path).unwrap() + point.0;
                let y = next_number(&mut i, &path).unwrap() + point.1;
                point = (x, y);
                commands.push(SvgCommand::M(Point::new(x, y, 0.0)));
            }
            SvgToken::L(true) => loop {
                let x = next_number(&mut i, &path).unwrap();
                let y = next_number(&mut i, &path).unwrap();
                point = (x, y);
                commands.push(SvgCommand::L(Point::new(x, y, 0.0)));
                if !is_number(path.get(i)) {
                    break;
                }
            },
            SvgToken::L(false) => loop {
                let x = next_number(&mut i, &path).unwrap() + point.0;
                let y = next_number(&mut i, &path).unwrap() + point.1;
                point = (x, y);
                commands.push(SvgCommand::L(Point::new(x, y, 0.0)));
                if !is_number(path.get(i)) {
                    break;
                }
            },
            SvgToken::H(true) => {
                let x = next_number(&mut i, &path).unwrap();
                point = (x, point.1);
                commands.push(SvgCommand::L(Point::new(point.0, point.1, 0.0)));
            }
            SvgToken::H(false) => {
                let x = next_number(&mut i, &path).unwrap() + point.0;
                point = (x, point.1);
                commands.push(SvgCommand::L(Point::new(point.0, point.1, 0.0)));
            }
            SvgToken::V(true) => {
                let y = next_number(&mut i, &path).unwrap();
                point = (point.0, y);
                commands.push(SvgCommand::L(Point::new(point.0, point.1, 0.0)));
            }
            SvgToken::V(false) => {
                let y = next_number(&mut i, &path).unwrap() + point.1;
                point = (point.0, y);
                commands.push(SvgCommand::L(Point::new(point.0, point.1, 0.0)));
            }
            SvgToken::Q(true) => loop {
                let x1 = next_number(&mut i, &path).unwrap();
                let y1 = next_number(&mut i, &path).unwrap();
                let x2 = next_number(&mut i, &path).unwrap();
                let y2 = next_number(&mut i, &path).unwrap();
                point = (x2, y2);
                commands.push(SvgCommand::Q(
                    Point::new(x1, y1, 0.0),
                    Point::new(x2, y2, 0.0),
                ));
                if !is_number(path.get(i)) {
                    break;
                }
            },
            SvgToken::Q(false) => loop {
                let x1 = next_number(&mut i, &path).unwrap() + point.0;
                let y1 = next_number(&mut i, &path).unwrap() + point.1;
                let x2 = next_number(&mut i, &path).unwrap() + point.0;
                let y2 = next_number(&mut i, &path).unwrap() + point.1;
                point = (x2, y2);
                commands.push(SvgCommand::Q(
                    Point::new(x1, y1, 0.0),
                    Point::new(x2, y2, 0.0),
                ));
                if !is_number(path.get(i)) {
                    break;
                }
            },
            SvgToken::T(true) => loop {
                let x2 = next_number(&mut i, &path).unwrap();
                let y2 = next_number(&mut i, &path).unwrap();
                point = (x2, y2);
                commands.push(SvgCommand::T(Point::new(x2, y2, 0.0)));
                if !is_number(path.get(i)) {
                    break;
                }
            },
            SvgToken::T(false) => loop {
                let x2 = next_number(&mut i, &path).unwrap() + point.0;
                let y2 = next_number(&mut i, &path).unwrap() + point.1;
                point = (x2, y2);
                commands.push(SvgCommand::T(Point::new(x2, y2, 0.0)));
                if !is_number(path.get(i)) {
                    break;
                }
            },
            SvgToken::C(true) => loop {
                let x1 = next_number(&mut i, &path).unwrap();
                let y1 = next_number(&mut i, &path).unwrap();
                let x2 = next_number(&mut i, &path).unwrap();
                let y2 = next_number(&mut i, &path).unwrap();
                let x3 = next_number(&mut i, &path).unwrap();
                let y3 = next_number(&mut i, &path).unwrap();
                point = (x3, y3);
                commands.push(SvgCommand::C(
                    Point::new(x1, y1, 0.0),
                    Point::new(x2, y2, 0.0),
                    Point::new(x3, y3, 0.0),
                ));
                if !is_number(path.get(i)) {
                    break;
                }
            },
            SvgToken::C(false) => loop {
                let x1 = next_number(&mut i, &path).unwrap() + point.0;
                let y1 = next_number(&mut i, &path).unwrap() + point.1;
                let x2 = next_number(&mut i, &path).unwrap() + point.0;
                let y2 = next_number(&mut i, &path).unwrap() + point.1;
                let x3 = next_number(&mut i, &path).unwrap() + point.0;
                let y3 = next_number(&mut i, &path).unwrap() + point.1;
                point = (x3, y3);
                commands.push(SvgCommand::C(
                    Point::new(x1, y1, 0.0),
                    Point::new(x2, y2, 0.0),
                    Point::new(x3, y3, 0.0),
                ));
                if !is_number(path.get(i)) {
                    break;
                }
            },
            SvgToken::S(true) => loop {
                let x2 = next_number(&mut i, &path).unwrap();
                let y2 = next_number(&mut i, &path).unwrap();
                let x3 = next_number(&mut i, &path).unwrap();
                let y3 = next_number(&mut i, &path).unwrap();
                point = (x3, y3);
                commands.push(SvgCommand::S(
                    Point::new(x2, y2, 0.0),
                    Point::new(x3, y3, 0.0),
                ));
                if !is_number(path.get(i)) {
                    break;
                }
            },
            SvgToken::S(false) => loop {
                let x2 = next_number(&mut i, &path).unwrap() + point.0;
                let y2 = next_number(&mut i, &path).unwrap() + point.1;
                let x3 = next_number(&mut i, &path).unwrap() + point.0;
                let y3 = next_number(&mut i, &path).unwrap() + point.1;
                point = (x3, y3);
                commands.push(SvgCommand::S(
                    Point::new(x2, y2, 0.0),
                    Point::new(x3, y3, 0.0),
                ));
                if !is_number(path.get(i)) {
                    break;
                }
            },
            SvgToken::A(true) => loop {
                let rx = next_number(&mut i, &path).unwrap();
                let ry = next_number(&mut i, &path).unwrap();
                let rotation = next_number(&mut i, &path).unwrap();
                let large_arc = next_number(&mut i, &path).unwrap() != 0.0;
                let sweep = next_number(&mut i, &path).unwrap() != 0.0;
                let px = next_number(&mut i, &path).unwrap();
                let py = next_number(&mut i, &path).unwrap();
                point = (px, py);
                commands.push(SvgCommand::A(
                    Vector::new(rx, ry, 0.0),
                    rotation,
                    large_arc,
                    sweep,
                    Point::new(px, py, 0.0),
                ));
                if !is_number(path.get(i)) {
                    break;
                }
            },
            SvgToken::A(false) => loop {
                let rx = next_number(&mut i, &path).unwrap();
                let ry = next_number(&mut i, &path).unwrap();
                let rotation = next_number(&mut i, &path).unwrap();
                let large_arc = next_number(&mut i, &path).unwrap() != 0.0;
                let sweep = next_number(&mut i, &path).unwrap() != 0.0;
                let px = next_number(&mut i, &path).unwrap() + point.0;
                let py = next_number(&mut i, &path).unwrap() + point.1;
                point = (px, py);
                commands.push(SvgCommand::A(
                    Vector::new(rx, ry, 0.0),
                    rotation,
                    large_arc,
                    sweep,
                    Point::new(px, py, 0.0),
                ));
                if !is_number(path.get(i)) {
                    break;
                }
            },
            SvgToken::Z => {
                commands.push(SvgCommand::Z);
            }
            SvgToken::Number(_) => return None,
        };
    }
    Some(commands)
}

fn next_number(i: &mut usize, tokens: &Vec<SvgToken>) -> Option<f64> {
    match tokens.get(*i) {
        Some(SvgToken::Number(x)) => {
            *i += 1;
            Some(*x)
        }
        _ => None,
    }
}

fn is_number(token: Option<&SvgToken>) -> bool {
    if let Some(SvgToken::Number(_)) = token {
        true
    } else {
        false
    }
}
