use crate::constants::svg;
use crate::svg_tools::Svg;
use crate::wasm::Canvas;
use graphics::{shade_faces, Camera, LightSource, Matrix, Object, Point};

pub enum Entity<T: Copy> {
    Shape(Object),
    Svg {
        svg: Svg,
        t: Matrix,
        use_camera: bool,
        on_click: Option<Box<dyn Fn()>>,
        category: Option<T>,
    },
    BoundingBox([[f64; 2]; 4], Option<Box<dyn Fn()>>, Option<T>),
}

impl<T: Copy> Entity<T> {
    pub fn draw(&self, canvas: &Canvas, camera: &Option<Camera>, light: &Option<LightSource>) {
        match self {
            Entity::Shape(shape) => {
                let camera = camera.as_ref().unwrap();
                let light = light.as_ref().unwrap();
                let mut face_list = shade_faces(&vec![*shape], camera, light);
                face_list.sort_by(|x, y| x.0.partial_cmp(&y.0).unwrap());
                for (_, points, color) in face_list {
                    canvas.begin_path();
                    canvas.move_to(points[0][0], points[0][1]);
                    for i in 1..points.len() {
                        canvas.line_to(points[i][0], points[i][1]);
                    }
                    canvas.close_path();
                    let style = format!("rgba({}, {}, {}, 255)", color[0], color[1], color[2]);
                    canvas.set_stroke_style(&style);
                    canvas.set_fill_style(&style);
                    canvas.stroke();
                    canvas.fill();
                }
            }
            Entity::Svg {
                svg, t, use_camera, ..
            } => {
                if *use_camera {
                    svg.draw(canvas, *t, camera.as_ref());
                } else {
                    svg.draw(canvas, *t, None);
                }
            }
            Entity::BoundingBox(..) => (),
        }
    }

    pub fn check_hit(&self, x: i32, y: i32, camera: &Option<Camera>) -> Option<T> {
        match self {
            Entity::Shape(_) => None,
            Entity::Svg {
                t,
                use_camera,
                category,
                ..
            } => {
                if hit_svg(x, y, camera, *t, *use_camera) {
                    *category
                } else {
                    None
                }
            }
            Entity::BoundingBox(points, _, category) => {
                if hit_box(x, y, points) {
                    *category
                } else {
                    None
                }
            }
        }
    }

    pub fn click(&self, x: i32, y: i32, camera: &Option<Camera>) -> bool {
        match self {
            Entity::Shape(_) => false,
            Entity::Svg {
                t,
                use_camera,
                on_click,
                ..
            } => {
                if let Some(on_click) = on_click {
                    let hit = hit_svg(x, y, camera, *t, *use_camera);
                    if hit {
                        (on_click)();
                    }
                    hit
                } else {
                    false
                }
            }
            Entity::BoundingBox(points, on_click, _) => {
                if let Some(on_click) = on_click {
                    let hit = hit_box(x, y, points);
                    if hit {
                        (on_click)();
                    }
                    hit
                } else {
                    false
                }
            }
        }
    }
}

fn hit_svg(x: i32, y: i32, camera: &Option<Camera>, t: Matrix, use_camera: bool) -> bool {
    let points = &mut [
        t * Point::new(0.0, 0.0, 0.0),
        t * Point::new(svg::SIZE, 0.0, 0.0),
        t * Point::new(svg::SIZE, svg::SIZE, 0.0),
        t * Point::new(0.0, svg::SIZE, 0.0),
    ];
    let points = if use_camera {
        let camera = camera.as_ref().unwrap();
        [
            camera.world_to_pixel(points[0]),
            camera.world_to_pixel(points[1]),
            camera.world_to_pixel(points[2]),
            camera.world_to_pixel(points[3]),
        ]
    } else {
        [
            [points[0][0], points[0][1]],
            [points[1][0], points[1][1]],
            [points[2][0], points[2][1]],
            [points[3][0], points[3][1]],
        ]
    };
    hit_box(x, y, &points)
}

fn hit_box(x: i32, y: i32, points: &[[f64; 2]; 4]) -> bool {
    let mut sign = false;
    let mut hit = true;
    for i in 0..4 {
        let p1 = points[i];
        let p2 = points[(i + 1) % 4];
        // (x-x1) * (y1-y2) + (y-y1) * (x2-x1)
        let s = (x - p1[0] as i32) * (p1[1] as i32 - p2[1] as i32)
            + (y - p1[1] as i32) * (p2[0] as i32 - p1[0] as i32)
            > 0;
        if i == 0 {
            sign = s;
        } else if sign != s {
            hit = false;
            break;
        }
    }
    hit
}
