use crate::wasm::Canvas;
pub use entity::Entity;
use graphics::{Camera, LightSource};

mod entity;

pub struct Scene<T: Copy> {
    pub objects: Vec<Entity<T>>,
    pub camera: Option<Camera>,
    pub light: Option<LightSource>,
}

impl<T: Copy> Scene<T> {
    pub fn new(camera: Option<Camera>, light: Option<LightSource>) -> Scene<T> {
        Scene {
            objects: Vec::new(),
            camera,
            light,
        }
    }

    pub fn push(&mut self, object: Entity<T>) {
        self.objects.push(object);
    }

    pub fn draw(&self, canvas: &Canvas) {
        for o in &self.objects {
            o.draw(canvas, &self.camera, &self.light);
        }
    }

    pub fn check_coords(&self, x: i32, y: i32) -> Option<T> {
        for o in self.objects.iter().rev() {
            let x = o.check_hit(x, y, &self.camera);
            if x.is_some() {
                return x;
            }
        }
        None
    }

    pub fn click(&self, x: i32, y: i32) {
        for o in self.objects.iter().rev() {
            if o.click(x, y, &self.camera) {
                break;
            }
        }
    }
}
