use crate::constants::{events, ANIMATION_STEP_DURATION, CANVAS_ID};
use crate::logic::{listeners, open_ws, ping, state};
use crate::render::draw;
use crate::wasm::{add_callback, set_interval, Canvas};
use wasm_bindgen::prelude::wasm_bindgen;

#[macro_use]
extern crate lazy_static;

#[macro_export]
macro_rules! console_log {
    ($($t:tt)*) => (web_sys::console::log_1(&wasm_bindgen::JsValue::from(&format_args!($($t)*).to_string())))
}

mod constants;
mod logic;
mod render;
mod scene;
mod svg_tools;
pub mod wasm;

#[wasm_bindgen(start)]
pub fn main() {
    let window = web_sys::window().unwrap();
    let canvas = Canvas::from_id(CANVAS_ID).unwrap().canvas();

    let resize: Box<dyn Fn()> = Box::new(&draw);
    let mouse_down: Box<dyn Fn(web_sys::MouseEvent)> = Box::new(&listeners::mouse_down);
    let mouse_move: Box<dyn Fn(web_sys::MouseEvent)> = Box::new(&listeners::mouse_move);
    let mouse_up: Box<dyn Fn()> = Box::new(&listeners::touch_end);
    let touch_start: Box<dyn Fn(web_sys::TouchEvent)> = Box::new(&listeners::touch_start);
    let touch_move: Box<dyn Fn(web_sys::TouchEvent)> = Box::new(&listeners::touch_move);
    let touch_end: Box<dyn Fn()> = Box::new(&listeners::touch_end);

    add_callback(resize, &window, events::RESIZE);
    add_callback(mouse_down, &canvas, events::MOUSE_DOWN);
    add_callback(mouse_move, &canvas, events::MOUSE_MOVE);
    add_callback(mouse_up, &canvas, events::MOUSE_UP);
    add_callback(touch_start, &canvas, events::TOUCH_START);
    add_callback(touch_move, &canvas, events::TOUCH_MOVE);
    add_callback(touch_end, &canvas, events::TOUCH_END);

    draw();
    open_ws();
    set_interval(Box::new(&animation_step), ANIMATION_STEP_DURATION);
    set_interval(Box::new(&ping), 1000);
}

fn animation_step() {
    if state().tick() {
        console_log!("drawing");
        draw();
    }
}
