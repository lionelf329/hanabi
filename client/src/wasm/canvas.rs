use wasm_bindgen::{JsCast, JsValue};

pub struct Canvas {
    context: web_sys::CanvasRenderingContext2d,
}

impl Canvas {
    pub fn from_id(id: &str) -> Option<Canvas> {
        let window = web_sys::window().expect("no global `window` exists");
        let document = window.document().expect("should have a document on window");
        let element = match document.get_element_by_id(id) {
            Some(e) => e,
            _ => return None,
        };
        let canvas = match element.dyn_into::<web_sys::HtmlCanvasElement>() {
            Ok(c) => c,
            _ => return None,
        };
        let context = match canvas.get_context("2d") {
            Ok(Some(c)) => c,
            _ => return None,
        };
        let context = match context.dyn_into::<web_sys::CanvasRenderingContext2d>() {
            Ok(c) => c,
            _ => return None,
        };
        Some(Canvas { context })
    }

    pub fn set_width(&self, w: u32) {
        self.context.canvas().unwrap().set_width(w)
    }

    pub fn set_height(&self, h: u32) {
        self.context.canvas().unwrap().set_height(h)
    }

    pub fn canvas(&self) -> web_sys::HtmlCanvasElement {
        self.context.canvas().unwrap()
    }

    pub fn global_alpha(&self) -> f64 {
        self.context.global_alpha()
    }

    pub fn set_global_alpha(&self, value: f64) {
        self.context.set_global_alpha(value)
    }

    pub fn global_composite_operation(&self) -> Result<String, JsValue> {
        self.context.global_composite_operation()
    }

    pub fn set_global_composite_operation(&self, value: &str) -> Result<(), JsValue> {
        self.context.set_global_composite_operation(value)
    }

    pub fn stroke_style(&self) -> String {
        self.context.stroke_style().as_string().unwrap()
    }

    pub fn set_stroke_style(&self, value: &str) {
        self.context.set_stroke_style(&JsValue::from(value))
    }

    pub fn fill_style(&self) -> String {
        self.context.fill_style().as_string().unwrap()
    }

    pub fn set_fill_style(&self, value: &str) {
        self.context.set_fill_style(&JsValue::from(value))
    }

    pub fn set_gradient(&self, p1: [f64; 2], p2: [f64; 2], colors: &[(f32, String)]) {
        let gradient = self.create_linear_gradient(p1[0], p1[1], p2[0], p2[1]);
        for (position, color) in colors.iter() {
            gradient.add_color_stop(*position, color).unwrap();
        }
        self.context.set_fill_style(&gradient);
    }

    pub fn filter(&self) -> String {
        self.context.filter()
    }

    pub fn set_filter(&self, value: &str) {
        self.context.set_filter(value)
    }

    pub fn image_smoothing_enabled(&self) -> bool {
        self.context.image_smoothing_enabled()
    }

    pub fn set_image_smoothing_enabled(&self, value: bool) {
        self.context.set_image_smoothing_enabled(value)
    }

    pub fn line_width(&self) -> f64 {
        self.context.line_width()
    }

    pub fn set_line_width(&self, value: f64) {
        self.context.set_line_width(value)
    }

    pub fn line_cap(&self) -> String {
        self.context.line_cap()
    }

    pub fn set_line_cap(&self, value: &str) {
        self.context.set_line_cap(value)
    }

    pub fn line_join(&self) -> String {
        self.context.line_join()
    }

    pub fn set_line_join(&self, value: &str) {
        self.context.set_line_join(value)
    }

    pub fn miter_limit(&self) -> f64 {
        self.context.miter_limit()
    }

    pub fn set_miter_limit(&self, value: f64) {
        self.context.set_miter_limit(value)
    }

    pub fn line_dash_offset(&self) -> f64 {
        self.context.line_dash_offset()
    }

    pub fn set_line_dash_offset(&self, value: f64) {
        self.context.set_line_dash_offset(value)
    }

    pub fn shadow_offset_x(&self) -> f64 {
        self.context.shadow_offset_x()
    }

    pub fn set_shadow_offset_x(&self, value: f64) {
        self.context.set_shadow_offset_x(value)
    }

    pub fn shadow_offset_y(&self) -> f64 {
        self.context.shadow_offset_y()
    }

    pub fn set_shadow_offset_y(&self, value: f64) {
        self.context.set_shadow_offset_y(value)
    }

    pub fn shadow_blur(&self) -> f64 {
        self.context.shadow_blur()
    }

    pub fn set_shadow_blur(&self, value: f64) {
        self.context.set_shadow_blur(value)
    }

    pub fn shadow_color(&self) -> String {
        self.context.shadow_color()
    }

    pub fn set_shadow_color(&self, value: &str) {
        self.context.set_shadow_color(value)
    }

    pub fn font(&self) -> String {
        self.context.font()
    }

    pub fn set_font(&self, value: &str) {
        self.context.set_font(value)
    }

    pub fn text_align(&self) -> String {
        self.context.text_align()
    }

    pub fn set_text_align(&self, value: &str) {
        self.context.set_text_align(value)
    }

    pub fn text_baseline(&self) -> String {
        self.context.text_baseline()
    }

    pub fn set_text_baseline(&self, value: &str) {
        self.context.set_text_baseline(value)
    }

    pub fn draw_window(
        &self,
        window: &web_sys::Window,
        x: f64,
        y: f64,
        w: f64,
        h: f64,
        bg_color: &str,
    ) -> Result<(), JsValue> {
        self.context.draw_window(window, x, y, w, h, bg_color)
    }

    pub fn draw_window_with_flags(
        &self,
        window: &web_sys::Window,
        x: f64,
        y: f64,
        w: f64,
        h: f64,
        bg_color: &str,
        flags: u32,
    ) -> Result<(), JsValue> {
        self.context
            .draw_window_with_flags(window, x, y, w, h, bg_color, flags)
    }

    pub fn draw_image_with_html_canvas_element(
        &self,
        image: &web_sys::HtmlCanvasElement,
        dx: f64,
        dy: f64,
    ) -> Result<(), JsValue> {
        self.context
            .draw_image_with_html_canvas_element(image, dx, dy)
    }

    pub fn draw_image_with_html_canvas_element_and_dw_and_dh(
        &self,
        image: &web_sys::HtmlCanvasElement,
        dx: f64,
        dy: f64,
        dw: f64,
        dh: f64,
    ) -> Result<(), JsValue> {
        self.context
            .draw_image_with_html_canvas_element_and_dw_and_dh(image, dx, dy, dw, dh)
    }

    pub fn draw_image_with_html_canvas_element_and_sw_and_sh_and_dx_and_dy_and_dw_and_dh(
        &self,
        image: &web_sys::HtmlCanvasElement,
        sx: f64,
        sy: f64,
        sw: f64,
        sh: f64,
        dx: f64,
        dy: f64,
        dw: f64,
        dh: f64,
    ) -> Result<(), JsValue> {
        self.context
            .draw_image_with_html_canvas_element_and_sw_and_sh_and_dx_and_dy_and_dw_and_dh(
                image, sx, sy, sw, sh, dx, dy, dw, dh,
            )
    }

    pub fn begin_path(&self) {
        self.context.begin_path()
    }

    pub fn clip(&self) {
        self.context.clip()
    }

    pub fn clip_with_canvas_winding_rule(&self, winding: web_sys::CanvasWindingRule) {
        self.context.clip_with_canvas_winding_rule(winding)
    }

    pub fn clip_with_path_2d(&self, path: &web_sys::Path2d) {
        self.context.clip_with_path_2d(path)
    }

    pub fn clip_with_path_2d_and_winding(
        &self,
        path: &web_sys::Path2d,
        winding: web_sys::CanvasWindingRule,
    ) {
        self.context.clip_with_path_2d_and_winding(path, winding)
    }

    pub fn fill(&self) {
        self.context.fill()
    }

    pub fn fill_with_canvas_winding_rule(&self, winding: web_sys::CanvasWindingRule) {
        self.context.fill_with_canvas_winding_rule(winding)
    }

    pub fn fill_with_path_2d(&self, path: &web_sys::Path2d) {
        self.context.fill_with_path_2d(path)
    }

    pub fn fill_with_path_2d_and_winding(
        &self,
        path: &web_sys::Path2d,
        winding: web_sys::CanvasWindingRule,
    ) {
        self.context.fill_with_path_2d_and_winding(path, winding)
    }

    pub fn is_point_in_path_with_f64(&self, x: f64, y: f64) -> bool {
        self.context.is_point_in_path_with_f64(x, y)
    }

    pub fn is_point_in_path_with_f64_and_canvas_winding_rule(
        &self,
        x: f64,
        y: f64,
        winding: web_sys::CanvasWindingRule,
    ) -> bool {
        self.context
            .is_point_in_path_with_f64_and_canvas_winding_rule(x, y, winding)
    }

    pub fn is_point_in_path_with_path_2d_and_f64(
        &self,
        path: &web_sys::Path2d,
        x: f64,
        y: f64,
    ) -> bool {
        self.context
            .is_point_in_path_with_path_2d_and_f64(path, x, y)
    }

    pub fn is_point_in_path_with_path_2d_and_f64_and_winding(
        &self,
        path: &web_sys::Path2d,
        x: f64,
        y: f64,
        winding: web_sys::CanvasWindingRule,
    ) -> bool {
        self.context
            .is_point_in_path_with_path_2d_and_f64_and_winding(path, x, y, winding)
    }

    pub fn is_point_in_stroke_with_x_and_y(&self, x: f64, y: f64) -> bool {
        self.context.is_point_in_stroke_with_x_and_y(x, y)
    }

    pub fn is_point_in_stroke_with_path_and_x_and_y(
        &self,
        path: &web_sys::Path2d,
        x: f64,
        y: f64,
    ) -> bool {
        self.context
            .is_point_in_stroke_with_path_and_x_and_y(path, x, y)
    }

    pub fn stroke(&self) {
        self.context.stroke()
    }

    pub fn stroke_with_path(&self, path: &web_sys::Path2d) {
        self.context.stroke_with_path(path)
    }

    pub fn create_linear_gradient(
        &self,
        x0: f64,
        y0: f64,
        x1: f64,
        y1: f64,
    ) -> web_sys::CanvasGradient {
        self.context.create_linear_gradient(x0, y0, x1, y1)
    }

    pub fn create_pattern_with_html_canvas_element(
        &self,
        image: &web_sys::HtmlCanvasElement,
        repetition: &str,
    ) -> Result<Option<web_sys::CanvasPattern>, JsValue> {
        self.context
            .create_pattern_with_html_canvas_element(image, repetition)
    }

    pub fn create_radial_gradient(
        &self,
        x0: f64,
        y0: f64,
        r0: f64,
        x1: f64,
        y1: f64,
        r1: f64,
    ) -> Result<web_sys::CanvasGradient, JsValue> {
        self.context.create_radial_gradient(x0, y0, r0, x1, y1, r1)
    }

    pub fn add_hit_region(&self) -> Result<(), JsValue> {
        self.context.add_hit_region()
    }

    pub fn add_hit_region_with_options(
        &self,
        options: &web_sys::HitRegionOptions,
    ) -> Result<(), JsValue> {
        self.context.add_hit_region_with_options(options)
    }

    pub fn clear_hit_regions(&self) {
        self.context.clear_hit_regions()
    }

    pub fn remove_hit_region(&self, id: &str) {
        self.context.remove_hit_region(id)
    }

    pub fn get_line_dash(&self) -> js_sys::Array {
        self.context.get_line_dash()
    }

    pub fn set_line_dash(&self, segments: &JsValue) -> Result<(), JsValue> {
        self.context.set_line_dash(segments)
    }

    pub fn arc(
        &self,
        x: f64,
        y: f64,
        radius: f64,
        start_angle: f64,
        end_angle: f64,
    ) -> Result<(), JsValue> {
        self.context.arc(x, y, radius, start_angle, end_angle)
    }

    pub fn arc_with_anticlockwise(
        &self,
        x: f64,
        y: f64,
        radius: f64,
        start_angle: f64,
        end_angle: f64,
        anticlockwise: bool,
    ) -> Result<(), JsValue> {
        self.context
            .arc_with_anticlockwise(x, y, radius, start_angle, end_angle, anticlockwise)
    }

    pub fn arc_to(&self, x1: f64, y1: f64, x2: f64, y2: f64, radius: f64) -> Result<(), JsValue> {
        self.context.arc_to(x1, y1, x2, y2, radius)
    }

    pub fn bezier_curve_to(&self, cp1x: f64, cp1y: f64, cp2x: f64, cp2y: f64, x: f64, y: f64) {
        self.context.bezier_curve_to(cp1x, cp1y, cp2x, cp2y, x, y)
    }

    pub fn close_path(&self) {
        self.context.close_path()
    }

    pub fn ellipse(
        &self,
        x: f64,
        y: f64,
        radius_x: f64,
        radius_y: f64,
        rotation: f64,
        start_angle: f64,
        end_angle: f64,
    ) -> Result<(), JsValue> {
        self.context
            .ellipse(x, y, radius_x, radius_y, rotation, start_angle, end_angle)
    }

    pub fn ellipse_with_anticlockwise(
        &self,
        x: f64,
        y: f64,
        radius_x: f64,
        radius_y: f64,
        rotation: f64,
        start_angle: f64,
        end_angle: f64,
        anticlockwise: bool,
    ) -> Result<(), JsValue> {
        self.context.ellipse_with_anticlockwise(
            x,
            y,
            radius_x,
            radius_y,
            rotation,
            start_angle,
            end_angle,
            anticlockwise,
        )
    }

    pub fn line_to(&self, x: f64, y: f64) {
        self.context.line_to(x, y)
    }

    pub fn move_to(&self, x: f64, y: f64) {
        self.context.move_to(x, y)
    }

    pub fn quadratic_curve_to(&self, cpx: f64, cpy: f64, x: f64, y: f64) {
        self.context.quadratic_curve_to(cpx, cpy, x, y)
    }

    pub fn rect(&self, x: f64, y: f64, w: f64, h: f64) {
        self.context.rect(x, y, w, h)
    }

    pub fn clear_rect(&self, x: f64, y: f64, w: f64, h: f64) {
        self.context.clear_rect(x, y, w, h)
    }

    pub fn fill_rect(&self, x: f64, y: f64, w: f64, h: f64) {
        self.context.fill_rect(x, y, w, h)
    }

    pub fn stroke_rect(&self, x: f64, y: f64, w: f64, h: f64) {
        self.context.stroke_rect(x, y, w, h)
    }

    pub fn restore(&self) {
        self.context.restore()
    }

    pub fn save(&self) {
        self.context.save()
    }

    pub fn fill_text(&self, text: &str, x: f64, y: f64) -> Result<(), JsValue> {
        self.context.fill_text(text, x, y)
    }

    pub fn fill_text_with_max_width(
        &self,
        text: &str,
        x: f64,
        y: f64,
        max_width: f64,
    ) -> Result<(), JsValue> {
        self.context.fill_text_with_max_width(text, x, y, max_width)
    }

    pub fn measure_text(&self, text: &str) -> Result<web_sys::TextMetrics, JsValue> {
        self.context.measure_text(text)
    }

    pub fn stroke_text(&self, text: &str, x: f64, y: f64) -> Result<(), JsValue> {
        self.context.stroke_text(text, x, y)
    }

    pub fn stroke_text_with_max_width(
        &self,
        text: &str,
        x: f64,
        y: f64,
        max_width: f64,
    ) -> Result<(), JsValue> {
        self.context
            .stroke_text_with_max_width(text, x, y, max_width)
    }

    pub fn rotate(&self, angle: f64) -> Result<(), JsValue> {
        self.context.rotate(angle)
    }

    pub fn scale(&self, x: f64, y: f64) -> Result<(), JsValue> {
        self.context.scale(x, y)
    }

    pub fn translate(&self, x: f64, y: f64) -> Result<(), JsValue> {
        self.context.translate(x, y)
    }

    pub fn draw_custom_focus_ring(&self, element: &web_sys::Element) -> bool {
        self.context.draw_custom_focus_ring(element)
    }

    pub fn draw_focus_if_needed(&self, element: &web_sys::Element) -> Result<(), JsValue> {
        self.context.draw_focus_if_needed(element)
    }
}
