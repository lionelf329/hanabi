## Todo

Although it is already completely playable, and as
far as I can tell, bug free, there are still things
that could be added to make it even better.

#### Scalability
1. Garbage collect games that everyone has left

#### New features
1. See hints other people have received
1. See recent actions

#### Clearer interface
1. Make it more clear when it's your turn
1. Make hints disappear at a more consistent time
1. Make websocket reload smoother

#### Platform support
1. Key events for typing code
1. Escape to zoom out
