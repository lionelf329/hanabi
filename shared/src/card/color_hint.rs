use super::{count_false, count_true};
use crate::constants::COLORS_WITHOUT_RAINBOW;

#[derive(Copy, Clone, Debug)]
pub enum ColorHintStatus {
    /// No hits yet
    SomeEliminated([bool; COLORS_WITHOUT_RAINBOW]),
    /// One hit, no misses
    Ambiguous(usize),
    /// A hit on one color and a hit/miss on a different color
    Known,
}

impl std::fmt::Display for ColorHintStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ColorHintStatus::Known => f.write_str("X"),
            ColorHintStatus::Ambiguous(x) => f.write_str(&format!("{}", x)),
            ColorHintStatus::SomeEliminated(x) => {
                for i in x.iter() {
                    f.write_str(if *i { "1" } else { "0" }).unwrap();
                }
                Ok(())
            }
        }
    }
}

impl From<&str> for ColorHintStatus {
    fn from(str: &str) -> Self {
        if str == "X" {
            ColorHintStatus::Known
        } else if str.len() == 1 {
            ColorHintStatus::Ambiguous(str.parse::<usize>().unwrap())
        } else {
            let chars: Vec<char> = str.chars().collect();
            let mut colors = [false; 5];
            for i in 0..5 {
                colors[i] = chars[i] == '1';
            }
            ColorHintStatus::SomeEliminated(colors)
        }
    }
}

impl Default for ColorHintStatus {
    fn default() -> Self {
        ColorHintStatus::SomeEliminated([false; COLORS_WITHOUT_RAINBOW])
    }
}

impl ColorHintStatus {
    pub fn hint(&mut self, color: usize, hit: bool) {
        // if no hints given so far have applied to this card
        if let ColorHintStatus::SomeEliminated(eliminated) = self {
            // if the current hint applies to this card
            if hit {
                // if there have been hints then you know the color, otherwise it might be rainbow
                *self = if count_true(eliminated) > 0 {
                    ColorHintStatus::Known
                } else {
                    ColorHintStatus::Ambiguous(color)
                };
            } else {
                // indicate that it's not this color. If there's only one color left, it's that one
                eliminated[color - 1] = true;
                if count_false(eliminated) == 1 {
                    *self = ColorHintStatus::Known
                }
            }
        } else if let ColorHintStatus::Ambiguous(c) = self {
            // if it is either a particular color or rainbow and the current hint is a different
            // color, that will always narrow it down to one possibility
            if *c != color {
                *self = ColorHintStatus::Known;
            }
        }
    }
}
