pub const CODE_LENGTH: usize = 5;
pub const CODE_DIGIT_OPTIONS: usize = 5;

pub const CARD_DISTRIBUTION: &[usize] = &[1, 1, 1, 2, 2, 3, 3, 4, 4, 5];
pub const INITIAL_HINTS: usize = 8;
pub const INITIAL_FUSES: usize = 3;
pub const MAX_HINTS: usize = 8;
pub const STACK_SIZE: usize = 5;

pub const COLORS_WITH_RAINBOW: usize = 6;
pub const COLORS_WITHOUT_RAINBOW: usize = 5;

pub const WEBSOCKET_ROUTE: &str = "ws";
