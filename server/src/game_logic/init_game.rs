use crate::constants::*;
use crate::game_logic::{Game, Player};
use hanabi_shared::constants::*;
use hanabi_shared::Card;
use rand::rngs::ThreadRng;
use rand::seq::SliceRandom;
use rand::Rng;
use std::collections::{HashMap, HashSet};

pub fn new_game(games: &mut HashMap<usize, Game>, player_count: usize, rainbow: bool) -> usize {
    let mut rng = rand::thread_rng();
    let cards_per_player = cards_per_player(player_count);
    let color_count = color_count(rainbow);

    let code = random_code(games, &mut rng);

    let mut remaining = shuffle_deck(&mut rng, color_count);

    let mut players = Vec::with_capacity(player_count);
    for _ in 0..player_count {
        players.push(Player {
            user_id: None,
            cards: remaining.split_off(remaining.len() - cards_per_player),
        });
    }

    games.insert(
        code,
        Game {
            users: HashSet::new(),
            remaining,
            players,
            discarded: Vec::with_capacity(color_count * CARD_DISTRIBUTION.len()),
            stacks: vec![0; color_count],
            hints: INITIAL_HINTS,
            fuses: INITIAL_FUSES,
            turn: 0,
        },
    );
    code
}

fn random_code(games: &mut HashMap<usize, Game>, rng: &mut ThreadRng) -> usize {
    let mut code: usize;
    loop {
        code = rng.gen_range(0..CODE_DIGIT_OPTIONS.pow(CODE_LENGTH as u32));
        if !games.contains_key(&code) {
            break;
        }
    }
    code
}

fn shuffle_deck(rng: &mut ThreadRng, color_count: usize) -> Vec<Card> {
    let mut cards: Vec<Card> = Vec::with_capacity(color_count * CARD_DISTRIBUTION.len());
    for color in 1..=color_count {
        for &number in CARD_DISTRIBUTION.iter() {
            cards.push(Card::new(color, number));
        }
    }
    cards.shuffle(rng);
    cards
}

fn color_count(rainbow: bool) -> usize {
    if rainbow {
        COLORS_WITH_RAINBOW
    } else {
        COLORS_WITHOUT_RAINBOW
    }
}
