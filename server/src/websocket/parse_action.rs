use hanabi_shared::Hint;

pub enum WsAction {
    Discard(usize),
    Hint(usize, Hint),
    Host(usize),
    Join(usize),
    Play(usize),
    Spectate(usize),
}

pub fn parse(message: &str) -> Option<WsAction> {
    let msg: Vec<&str> = message.split_whitespace().collect();
    match (*msg.get(0)?, msg.len()) {
        ("host", 2) => {
            let player_count = usize_or_none(&msg, 1)?;
            Some(WsAction::Host(player_count))
        }
        ("spectate", 2) => {
            let game_id = usize_or_none(&msg, 1)?;
            Some(WsAction::Spectate(game_id))
        }
        ("join", 2) => {
            let player_index = usize_or_none(&msg, 1)?;
            Some(WsAction::Join(player_index))
        }
        ("play", 2) => {
            let card_index = usize_or_none(&msg, 1)?;
            Some(WsAction::Play(card_index))
        }
        ("discard", 2) => {
            let card_index = usize_or_none(&msg, 1)?;
            Some(WsAction::Discard(card_index))
        }
        ("hint", 4) => {
            let player_index = usize_or_none(&msg, 1)?;
            let value = usize_or_none(&msg, 3)?;
            let hint = match *msg.get(2)? {
                "color" => Hint::Color(value),
                "number" => Hint::Number(value),
                _ => return None,
            };
            Some(WsAction::Hint(player_index, hint))
        }
        _ => None,
    }
}

fn usize_or_none(msg: &Vec<&str>, index: usize) -> Option<usize> {
    match msg.get(index)?.parse::<usize>() {
        Ok(u) => Some(u),
        Err(_) => None,
    }
}
