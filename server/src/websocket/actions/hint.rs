use crate::game_logic::GameJoinStatus;
use crate::ArcState;
use hanabi_shared::Hint;

pub async fn hint(user_id: usize, state: &ArcState, target: usize, hint: Hint, message: &str) {
    let mut s = state.lock().await;
    if let GameJoinStatus::Joined(game_id, player) = s.users[&user_id].1 {
        let game = s.games.get_mut(&game_id).unwrap();
        let outcome = game.hint(player, target, hint);
        if outcome.is_ok() {
            s.broadcast(game_id, &message);
        }
    }
}
