use crate::game_logic::GameJoinStatus;
use crate::ArcState;

pub async fn discard(user_id: usize, state: &ArcState, card_index: usize) {
    let mut s = state.lock().await;
    if let GameJoinStatus::Joined(game_id, player) = s.users[&user_id].1 {
        let game = s.games.get_mut(&game_id).unwrap();
        let outcome = game.discard(player, card_index);
        if let Ok(new_card) = outcome {
            let message = &match new_card {
                Some(card) => format!("discard {} draw {}", card_index, card),
                None => format!("discard {}", card_index),
            };
            s.broadcast(game_id, &message);
        }
    }
}
