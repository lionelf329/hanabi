use crate::constants::RAINBOW;
use crate::game_logic::new_game;
use crate::ArcState;

pub async fn host(user_id: usize, state: &ArcState, player_count: usize) {
    let mut s = state.lock().await;
    let game_id = new_game(&mut s.games, player_count, RAINBOW);
    std::mem::drop(s);
    super::spectate::spectate(user_id, state, game_id).await;
}
